import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by joy on 6/7/17.
 */
public class BinaryArrayHelper {
    private int first, last, middle, size, binaryArray[];

    public BinaryArrayHelper() {
        this.first = 0;
        this.last = 0;
        this.middle = 0;
        this.size = 0;

        this.binaryArray = null;
    }


    //take input-
    public void sizeOfArray() {
        Scanner inputArraySize = new Scanner(System.in);
        System.out.println("Enter number of elements");

        this.size = inputArraySize.nextInt();

    }

    public void intializeArray() {

        this.binaryArray = new int[size];
        System.out.println("Enter " + size + " integers");
    }

    public void setArrayElements() {
        Scanner inputArrayElement = new Scanner(System.in);

        for (int counter = 0; counter < size; counter++) {
            System.out.println("Enter elements");
            binaryArray[counter] = inputArrayElement.nextInt();
        }
    }

    public void sortingElements() {
        Arrays.sort(binaryArray);
        System.out.println("Elements in sorted form  :");
        for (int number : binaryArray) {
            System.out.println(number);
        }
    }

    public void searchElement() {
        System.out.println("Enter value to find");
        Scanner inputArrayElement2Search = new Scanner(System.in);
        int search = inputArrayElement2Search.nextInt();

                first = 0;
                last = size - 1;
                middle = (first + last) / 2;

                while (first <= last) {
                    if (binaryArray[middle] < search)
                        first = middle + 1;
                    else if (binaryArray[middle] == search) {
                        System.out.println(search + " found at location " + (middle + 1) + ".");
                        break;

                    } else
                        last = middle - 1;

                    middle = (first + last) / 2;
                }
                if (first > last)
                    System.out.println(search + " is not present in the list.\n");
            }
        }


