/**
 * Created by joy on 6/7/17.
 */
public class Main {

    public static void main(String[] args) {

        BinaryArrayHelper binaryArrayHelper = new BinaryArrayHelper();

        binaryArrayHelper.sizeOfArray();
        binaryArrayHelper.intializeArray();
        binaryArrayHelper.setArrayElements();
        binaryArrayHelper.sortingElements();
        binaryArrayHelper.searchElement();

        }
}
